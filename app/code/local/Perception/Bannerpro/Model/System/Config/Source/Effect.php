<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2010 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 
/**
 * Used in creating options for Yes|No config value selection
 *
 */
class Perception_Bannerpro_Model_System_Config_Source_Effect
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
			array('value' => 'none', 'label'=>Mage::helper('bannerpro')->__('None')),
            array('value' => 'bounce', 'label'=>Mage::helper('bannerpro')->__('Bounce')),
            array('value' => 'shake', 'label'=>Mage::helper('bannerpro')->__('Shake')),
            array('value' => 'wobble', 'label'=>Mage::helper('bannerpro')->__('Wobble')),
            array('value' => 'bounceInLeft', 'label'=>Mage::helper('bannerpro')->__('Bounce In Left')),
            array('value' => 'bounceOutDown', 'label'=>Mage::helper('bannerpro')->__('Bounce Out Down')),
            array('value' => 'fadeIn', 'label'=>Mage::helper('bannerpro')->__('Fade In')),
			array('value' => 'fadeInLeftBig', 'label'=>Mage::helper('bannerpro')->__('Fade In left big')),
			array('value' => 'fadeInUpBig', 'label'=>Mage::helper('bannerpro')->__('Fade In Up Big')),			
			array('value' => 'fadeOutLeft', 'label'=>Mage::helper('bannerpro')->__('Fade Out Left')),
			array('value' => 'fadeOutUp', 'label'=>Mage::helper('bannerpro')->__('Fade Out Up')),			
			array('value' => 'rotateIn', 'label'=>Mage::helper('bannerpro')->__('Rotate In')),
			array('value' => 'rotateInUpRight', 'label'=>Mage::helper('bannerpro')->__('Rotate In Up Right')),
			array('value' => 'rotateOutUpLeft', 'label'=>Mage::helper('bannerpro')->__('Rotate Out Up Left')),
			array('value' => 'rollIn', 'label'=>Mage::helper('bannerpro')->__('Roll In')),
			array('value' => 'zoomInLeft', 'label'=>Mage::helper('bannerpro')->__('Zoom In Left')),
			array('value' => 'zoomOutDown', 'label'=>Mage::helper('bannerpro')->__('Zoom Out Down')),
			array('value' => 'slideInDown', 'label'=>Mage::helper('bannerpro')->__('Slide In Down')),
			array('value' => 'slideOutDown', 'label'=>Mage::helper('bannerpro')->__('Slide Out Down')),
			array('value' => 'flash', 'label'=>Mage::helper('bannerpro')->__('Flash')),
			array('value' => 'pulse', 'label'=>Mage::helper('bannerpro')->__('Pulse')),
			array('value' => 'rubberBand', 'label'=>Mage::helper('bannerpro')->__('Rubber Band')),
			array('value' => 'bounceIn', 'label'=>Mage::helper('bannerpro')->__('Bounce In')),
			array('value' => 'bounceInUp', 'label'=>Mage::helper('bannerpro')->__('Bounce In Up')),
			array('value' => 'bounceOutRight', 'label'=>Mage::helper('bannerpro')->__('Bounce Out Right')),
			array('value' => 'fadeInUp', 'label'=>Mage::helper('bannerpro')->__('Fade In Up')),
			array('value' => 'slideInUp', 'label'=>Mage::helper('bannerpro')->__('Slide In Up'))
        );
    } 
}
