(function($){

	'use strict';

	function initTooltip(){
		var links         =  $('a[title]');
		var linkTitle   = links.attr('title');
		var linkTooltip = links.attr('data-toggle','tooltip').attr( 'data-original', linkTitle );

		linkTooltip.tooltip().on('hide.bs.tooltip', function( evt ) {
			evt.preventDefault();
			$('div.tooltip').fadeOut();
		});

		linkTooltip.tooltip().on('show.bs.tooltip', function( evt ) {
			evt.preventDefault();
			$('div.tooltip').fadeIn();
		});
	};

	function initEvents(){
		$('.product-image-thumbs').on('click', '.thumb-link', function(){
			var anchorID = $(this).attr('data-image-index');
			var image    = $('.product-image-zoom').find('#image-' + anchorID).not('src');
			image.attr('src', image.attr('data-zoom-image'));
		});		

		$(window).scroll(function(){
			var topDistance = $(window).scrollTop();
			if (topDistance > 38.18){
            	$('.dg-header').addClass('is-fixed');
	        }else{
	            $('.dg-header').removeClass('is-fixed');
	        }  
		});
	}

	function initLazyLoad(){
		if( $('[data-src]').length > 0 )
			$('[data-src]').lazyLoad();
	}

	function initCarousels(){

		if( $('.dg-newest-carousel').length > 0 ){
			$('.dg-newest-carousel').owlCarousel({
				responsive:{
					0: {
						items:1,
					},
					480: {
						items:2,
					},

					768: {
						items:3,
					},
					991: {
						items:4,
					}
				},
				navText: ['<i class="fa fa fa-angle-left"></i>','<i class="fa fa fa-angle-right"></i>'],
				margin: 30,
				dots: false,
				nav: true
			});
		}

		if( $('.js-slider-featured-products').length > 0) {
			$('.js-slider-featured-products').owlCarousel({
				items: 1,
				dots: true,
				nav: false,
				mouseDrag: false,
				touchDrag: false,
				navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
			});
		}

		if( $('.js-thumbnail-carousel').length > 0 ){

			if( $(window).width <= 768 ){
				$('.js-thumbnail-carousel').elastislide();
			}else{
				$('.js-thumbnail-carousel').elastislide({ orientation : 'vertical' });
			}
		}
	}

	function mobileFeatures(){

		var _windowWidth = $(window).width();

		var _deviceMobile       = 480;
		var _deviceTablet       = 768;
		var _deviceNotebook     = 1200;
		var _deviceDesktop      = 1366;
		var _deviceDesktopLarge = 1600;

		function footerMobile(){
			$('.dg-footer .block-links').find('.block-title').click(function( evt ){
				$(this).next().slideToggle();
				$(this).toggleClass('is-open')
			});

			$('.footer > div ').prepend( $('.newsletter-block').parent() );
		}

		function filterMobileSearch(){
			var buttonFilter = $('.block-layered-nav .block-subtitle');

			buttonFilter.on('click', function(){
				if( $(this).hasClass('active') ){
					$('.block-layered-nav').append( $('<div class="block-layered-backdrop"></div>') );
				}
			});

			$('.block-layered-nav').on('click', '.block-layered-backdrop', function(){
				closeFilterMobile();
			});

			$('.skip-link').click(function(){
				closeFilterMobile();
			});

			function closeFilterMobile(){
				closeMobileFilterMenu();
				removeMobileFilterBackdrop();
			}

			function closeMobileFilterMenu(){
				$('.block-layered-nav .block-subtitle').removeClass('active');
				$('.block-layered-nav #narrow-by-list').addClass('no-display');
			}

			function removeMobileFilterBackdrop(){
				$('.block-layered-backdrop').fadeOut(400).queue(function(){
					$(this).remove();
				});
			}
		}

		function removeAlertMessage(){
			$('.messages').click(function(){
				$(this).fadeOut(400).queue(function(){
					$(this).remove();
				})
			});
		}

		function moveCustomerPageBlocksToBottom(){
			if( $('.customer-account').length > 0 ){
				$('.col-main').after( $('.col-left.col-left-first') );
			}
		}

		function mobileCarousel(){
			if( $('.dg-carousel-thumbnail').length > 0 ){

				$('.dg-carousel-thumbnail')
				.addClass('owl-carousel owl-theme')
				.owlCarousel({
					items: 1,
					margin: 10,
					dots: false,
					nav: true,
					animateOut: 'slideOutUp',
					animateIn: 'slideInUp',
					responsive:{
						0: {
							items:3,
						},

						380: {
							items: 3,
						},

						768: {
							items:4,
						}
					},
					navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
				});
			}
		}
		
		if( _windowWidth <= _deviceTablet ){
			footerMobile();
			removeAlertMessage();
			filterMobileSearch();
			moveCustomerPageBlocksToBottom();
		}
	}


	function applyMasks(){

		var options = {
			clearIfNotMatch: true
		}

		$('input[name="postcode"], input[name="shipping[postcode]"], input[name="billing[postcode]"], input[name="estimate_postcode"]').mask('99999-999', options);
		$('input[name="taxvat"]').mask("999.999.999-99", options);
		$('input[name="telephone"], input[name="billing[telephone]"], input[name="shipping[telephone]"]').mask("(99) 9999-9999", options);
		$('input[name="fax"], input[name="shipping[fax]"], input[name="billing[fax]"]').mask("(99) 99999-9999", options);

		$('input[name="day"], input[name="month"]').mask("99");
		$('input[name="year"]').mask("9999");
	}

	$(window).ready(function(){
		initEvents();
		applyMasks();
		initLazyLoad();
		initCarousels();
		// initChanges();
		// initTooltip();

		if ( navigator.userAgent.match(/Android|iPad|Mobile|webOS|iPhone|iPod|Blackberry/i) != null ) {
			mobileFeatures();
		}
	});

}(jQuery));