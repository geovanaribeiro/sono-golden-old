
# note: this should never truly be referenced since we are using relative assets
# Mudar http_path para /skin/frontend/degrau/theme_project_name/

http_path = "/skin/frontend/degrau/spbrasil/"
css_dir = "../css"
sass_dir = "../scss"
images_dir = "../images"
javascripts_dir = "../js"
relative_assets = true
line_comments = false

output_style = :expanded
environment = :development

sourcemap = true
