/* ============================================ *
 * Buttons
 * ============================================ */

/* Primary Buttons */

.button,
.cart-table .product-cart-actions .button,
#co-shipping-method-form .buttons-set .button,
.cart .buttons-set .button,
.cart-table .button,
.sidebar .actions .button,
.button.button-secondary,
.cart-footer-actions .button2,
.button2 {
    border: 0;
    line-height: 1;
    // padding: 10px 15px;
    text-align: center;
    white-space: nowrap;
    display: inline-block;
    vertical-align: middle;
    padding: 17px 12px 12px 12px;
    background-color: $c-button;
    font-family: $f-stack-special;

    @include setProperty(border-radius, 3px);
    @include setTypography($f-size-s, $c-white, 600, uppercase);

    &:hover,
    &:focus {
        color: $c-white;
        cursor: pointer;
        background-color: $c-button-hover;
        border-color: $c-button-hover;
    }

    &:active {
        outline: 0;
        color: $c-white;
        border-color: $c-button-active;
        background-color: $c-button-active;
    }
}

/* Secondary Buttons */

// .cart .buttons-set .button,
// .cart-table .button,
// .sidebar .actions .button,
// .button.button-secondary,
// .cart-footer-actions .button2

.newsletter-block .button,
.button2 {
    cursor: pointer;
    font-family: $f-stack-special;
    background-color: $c-button-secondary;
    border-color: $c-button-secondary-border;

    @include setTypography($f-size, $c-white, 700, uppercase);

    &:hover {
        border-color: $c-button-secondary-hover;
        background-color: $c-button-secondary-hover;
    }

    &:active,
    &:focus {
        border-color: $c-button-secondary-active;
        background-color: $c-button-secondary-active;
    }
}

.sidebar .actions button.button {
    white-space: normal;
}

/* Disabled - class for anchor, state for form elements */

.button.disabled,
.button:disabled {
    opacity: 0.6;
    cursor: not-allowed;
    background-color: $c-button-disabled;
}

/* Adjacent buttons */

@media (min-width: $screen-sm) {
    .button + .button {
        margin-left: 5px;
    }
}

.btn-edit {
    &:before {
        content: "\E254";
        font-family: "Material Icons";
    }
}

.btn-cart {
    width: 125px;
    background-color: $c-white;
    border: 1px solid $c-divider-border;

    @include setTypography($f-size-s, $c-black, 600, uppercase);

    &:before {
        content: "\E8CC";
        margin-right: 5px;
        vertical-align: bottom;
        font-family: "Material Icons";

        @include setProperty(transition, color 0.3s);
        @include setTypography($f-size-l, $c-black, normal);
    }

    &:hover {
        color: $c-white;
        border-color: $c-terciary;
        background-color: $c-terciary;

        &:before {
            color: $c-white;
        }
    }
}

.btn-call-action {
    margin-top: 35px;
    padding: 14px 35px 10px 35px;
}

.back-link {
   a {
    color: $c-black;
    font-weight: 600;
    text-transform: uppercase;
    background-color: $c-white;
    padding: 5px 15px !important;
    border: 1px solid $c-primary;
    @include setProperty(border-radius, $default-border-radius);

    &:hover {
        color: $c-white;
        border-color: $c-primary;
        background-color: $c-primary;
    }
   } 
}

// .button2 {
//     border: 0;
//     padding: 0 5px;
//     margin: 0;
//     background: transparent;
//     cursor: pointer;
//     vertical-align: middle;
// }

// .button2:focus {
//     outline: none;
// }

// .button2 span,
// .button2 span span {
//     line-height: 30px;
//     height: 30px;
//     text-decoration: underline;
//     text-transform: uppercase;
//     display: inline-block;
//     color: $c-action;
//     font-family: $f-stack-special;

//     &:hover {
//         text-decoration: none;
//         color: $c-stimulus;
//     }
// }

@include bp(max-width, $screen-sm) {
    .col2-set .buttons-set {
        .button,
        .button2 {
            float: none;
            width: 100%;
            margin-left: 0;
            margin-right: 0;
        }

        .back-link {
            display: none;
        }

        .required {
            display: none;
        }
    }
}


@include bp(max-width, $screen-xs) {
    .buttons-set {
        .button {
            float: none;
            width: 100%;
            margin-left: 0;
            margin-right: 0;
            margin-bottom: $element-spacing;
        }

        .back-link {
            display: none;
        }

        .required {
            display: none;
        }
    }
}


/* -------------------------------------------- *
 * Paypal Button
 -------------------------------------------- */

.paypal-logo.paypal-after {
    float: left;
}

.paypal-after .paypal-or {
    float: left;
}

.paypal-or {
    line-height: 34px;
    margin: 0px 10px 5px;
}

.paypal-after .paypal-button {
    float: left;
}

.paypal-button {
    line-height: 0px;
}

.paypal-button img {
    display: inline;
}

@include bp(max-width, 740px) {
    .paypal-or {
        line-height: 20px;
    }

    .paypal-logo,
    .paypal-or,
    .paypal-button {
        text-align: center;
        width: 100%;
        display: block;
        margin-right: 0;
        margin-left: 0;
        float: none;
    }
}


/* -------------------------------------------- *
 * Button Sets
 * -------------------------------------------- */

.buttons-set {
    @include dgClearfix;

    // padding-top: $box-spacing;
    // border-top: 1px solid $c-module-border-light;
    text-align: left;

    p.required {
        margin: 0;
        margin-left: $gap;
        line-height: 33px;
        float: right;
    }

    .back-link {
        float: left;

        // margin: 0;
        // line-height: 33px;
   
    }

    a:not(.button) {
        line-height: 20px;
        display: inline-block;
        padding: 5px;
    }

    button.button {
        // float: right;
        // margin-left: 5px;
        // min-width: 140px;
   
    }

    &:after {
        @include clearfix;
    }
}

/* -------------------------------------------- *
 * Button - Remove / Previous
-------------------------------------------- */

.btn-remove,
.btn-previous {
    display: inline-block;
    width: 20px;
    height: 20px;

    // border: 1px solid $c-module-border-light;
    text-align: center;

    /* Hide text */
    font: 0/0 a;
    text-shadow: none;
    color: transparent;
    position: relative;
}

.btn-remove {
    &:after {
        font-size: 16px;
        color: $c-black;
        content: '\f00d';
        font-family: "fontAwesome";

        @include setProperty(transition, all 0.3s);
    }

    &:hover:after {
        color: $c-link-hover;
    }
}

.btn-remove2 {
    width: 19px;
    height: 24px;
    overflow: hidden;
    text-indent: -6px;

    &:after {
        top: 10px;
        content: "\E872";
        position: absolute;
        font-family: "Material Icons";

        @include setTypography(18px, $c-black, 300);
        @include setProperty(transition, color 0.3s);
    }

    &:hover {
        &:after {
            color: $c-secondary;
        }
    }
}

.btn-previous {
    &:after {
        @include triangle(left, 4px, $c-action);

        position: absolute;
        top: 50%;
        left: 50%;
        margin-left: -2px;
        margin-top: -4px;
    }

    &:hover:after {
        @include triangle(left, 4px, #FFFFFF);
    }
}

.block-layered-nav .currently,
.mini-products-list,
#compare-items {
    .btn-remove,
    .btn-previous {
        float: right;
        margin-left: 5px;
        position: relative;
    }
}